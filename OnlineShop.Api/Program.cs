using OnlineShop.Infrastructure;
using OnlineShop.Infrastructure.Core;
using OnlineShop.Infrastructure.Core.Notifications;
using OnlineShop.Infrastructure.DataAccess;
using OnlineShop.Infrastructure.Notifications;

namespace OnlineShop.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddMediatR(m =>
            {
                m.RegisterServicesFromAssemblies(typeof(BusinessLogic.CreateOrderRequestHandler).Assembly);
            });
            builder.Services.AddDbContext<AppDbContext>();
            builder.Services.AddScoped<IRepository, Repository>();
            builder.Services.AddScoped<INotificator, Notificator>();
            
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}