﻿using LanguageExt.ClassInstances;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using OnlineShop.Api.Responses;
using OnlineShop.Core.Modules.Orders.Cqrs;
using OnlineShop.Core.Modules.Orders.Models;

namespace OnlineShop.Api.Controllers
{
    [ApiController]
    [Route("orders")]
    public class OrdersController : Controller
    {
        private readonly IMediator _mediator;

        public OrdersController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpPost("create")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErrorResponse))]
        public async Task<IActionResult> Create([FromBody] CreateOrderRequest request)
        {
            var result = await _mediator.Send(request);
            return result.Match<IActionResult>(s => Ok(new DataResponse<Order>(s)),
                                               f => BadRequest(new ErrorResponse(f.Message)));
        }
    }
}
