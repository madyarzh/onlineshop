﻿using OnlineShop.Infrastructure.Core.Notifications.Parameters.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShop.Infrastructure.Core.Notifications.Parameters
{
    public class Notification
    {
        public NotificationType Type { get; set; }
    }
}
