﻿using OnlineShop.Infrastructure.Core.Notifications.Parameters;

namespace OnlineShop.Infrastructure.Core.Notifications
{
    public interface INotificator
    {
        void Send(Notification notification);
    }
}
