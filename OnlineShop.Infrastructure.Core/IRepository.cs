﻿using OnlineShop.Core.Shared;

namespace OnlineShop.Infrastructure.Core
{
    public interface IRepository
    {
        Task AddAsync(IDbModel entity);
        IQueryable<T> GetQuery<T>() where T : class;
    }
}