﻿using OnlineShop.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShop.Core.Modules.Orders.Models
{
    public class Order: IDbModel
    {
        public int Id { get; set; }
        public int BucketId { get; set; }
    }
}
