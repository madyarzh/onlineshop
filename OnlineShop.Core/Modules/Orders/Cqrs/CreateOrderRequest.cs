﻿using LanguageExt.Common;
using MediatR;
using OnlineShop.Core.Modules.Addresses.Models;
using OnlineShop.Core.Modules.Orders.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShop.Core.Modules.Orders.Cqrs
{
    public class CreateOrderRequest : IRequest<Result<Order>>
    {
        public int BucketId { get; set; }
        public decimal TotalPriceToCheck { get; set; }
    }
}
