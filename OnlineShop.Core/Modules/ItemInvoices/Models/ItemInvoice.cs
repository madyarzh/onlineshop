﻿using OnlineShop.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShop.Core.Modules.ItemInvoices.Models
{
    public class ItemInvoice: IDbModel
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public int WarehouseId { get; set; }
        public int AddressId { get; set; }
    }
}
