﻿using OnlineShop.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShop.Core.Modules.Buckets.Models
{
    public class Bucket: IDbModel
    {
        public int Id { get; set; }
        public bool IsPurchased { get; set; }
        public virtual ICollection<BucketItem> BucketItems { get; set; }
    }
}
