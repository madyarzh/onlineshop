﻿using OnlineShop.Core.Modules.Items.Models;
using OnlineShop.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShop.Core.Modules.Buckets.Models
{
    public class BucketItem: IDbModel
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public Item Item { get; set; }
        public int WarehouseId { get; set; }
        public int AddressId { get; set; }
        public decimal Price { get; set; }
    }
}
