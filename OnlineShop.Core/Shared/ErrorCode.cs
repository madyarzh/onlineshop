﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShop.Core.Shared
{
    public enum ErrorCode
    {
        entity_not_found = 1,
        price_changed = 2
    }
}
