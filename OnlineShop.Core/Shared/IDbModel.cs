﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShop.Core.Shared
{
    public interface IDbModel
    {
        int Id { get; set; }
    }
}
