﻿using Microsoft.EntityFrameworkCore;
using OnlineShop.Core.Modules.Addresses.Models;
using OnlineShop.Core.Modules.Buckets.Models;
using OnlineShop.Core.Modules.ItemInvoices.Models;
using OnlineShop.Core.Modules.Items.Models;
using OnlineShop.Core.Modules.Orders.Models;
using OnlineShop.Core.Modules.Prices.Models;
using OnlineShop.Core.Modules.Warehouses.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShop.Infrastructure.DataAccess
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Bucket> Buckets { get; set; }
        public DbSet<ItemInvoice> ItemInvoices { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
    }
}
