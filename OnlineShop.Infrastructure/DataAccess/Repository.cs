﻿using OnlineShop.Core.Shared;
using OnlineShop.Infrastructure.Core;

namespace OnlineShop.Infrastructure.DataAccess
{
    public class Repository : IRepository
    {
        private readonly AppDbContext _dbContext;

        public Repository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddAsync(IDbModel entity)
        {
            await _dbContext.AddAsync(entity);
        }

        public IQueryable<T> GetQuery<T>() where T:class
        {
            return _dbContext.Set<T>();
        }
        public Task SaveChangesAsync()
        {
            return _dbContext.SaveChangesAsync();
        }
    }
}