﻿using LanguageExt.Common;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OnlineShop.Core.Modules.Buckets.Models;
using OnlineShop.Core.Modules.ItemInvoices.Models;
using OnlineShop.Core.Modules.Orders.Cqrs;
using OnlineShop.Core.Modules.Orders.Models;
using OnlineShop.Core.Modules.Prices.Models;
using OnlineShop.Core.Modules.Warehouses.Models;
using OnlineShop.Core.Shared;
using OnlineShop.Infrastructure.Core;
using OnlineShop.Infrastructure.Core.Notifications;
using OnlineShop.Infrastructure.Core.Notifications.Parameters;
using OnlineShop.Infrastructure.Core.Notifications.Parameters.Enums;
using System.Linq;
namespace OnlineShop.BusinessLogic
{
    public class CreateOrderRequestHandler : IRequestHandler<CreateOrderRequest, Result<Order>>
    {
        private readonly IRepository _repository;
        private readonly INotificator _notificator;

        public CreateOrderRequestHandler(IRepository repository, INotificator notificator)
        {
            _repository = repository;
            _notificator = notificator;
        }

        public async Task<Result<Order>> Handle(CreateOrderRequest request, CancellationToken cancellationToken)
        {
            //Get all Items with Prices, sum prices and check with PriceToCheck
            var bucket = _repository.GetQuery<Bucket>()
                .Include(b => b.BucketItems)
                .FirstOrDefault(b => b.Id == request.BucketId);

            if (bucket is null)
                return new Result<Order>(new ApplicationException(ErrorCode.entity_not_found.ToString()));

            var itemIds = bucket.BucketItems.Select(bi => bi.ItemId).ToList();

            var prices = await _repository.GetQuery<Price>().Where(p => itemIds.Contains(p.ItemId)).ToArrayAsync(cancellationToken);

            var totalPrice = prices.Sum(p => p.Value);

            if (totalPrice != request.TotalPriceToCheck)
                return new Result<Order>(new ApplicationException(ErrorCode.price_changed.ToString()));

            //Create new order with selected Bucket 

            var newOrder = new Order
            {
                BucketId = bucket.Id
            };

            await _repository.AddAsync(newOrder);

            //Create Invoices for Items
            var invoices = bucket.BucketItems.Select(bi =>
                new ItemInvoice
                {
                    ItemId = bi.ItemId,
                    WarehouseId = bi.WarehouseId,
                    AddressId = bi.AddressId
                });

            //Send notification to Storage to move Item
            var warehouse = _repository.GetQuery<Warehouse>();

            _notificator.Send(new Notification
            {
                Type = NotificationType.Mail
            });
            return newOrder;
        }
    }
}