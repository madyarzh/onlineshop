﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace OnlineShop.Api.Tests.AssertionExceptions
{
    internal class HttpResponseMessageStatusAssertionException : Exception, IAssertionException
    {
        public HttpResponseMessageStatusAssertionException(string? message) : base(message)
        {
        }
    }
}
