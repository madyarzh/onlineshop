﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using OnlineShop.Infrastructure.DataAccess;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShop.Api.Tests
{
    public class TestWebApplicationFactory<TProgram>
    : WebApplicationFactory<TProgram> where TProgram : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var dbContextOptionsDescriptor = services.Single(
                    d => d.ServiceType ==
                        typeof(DbContextOptions<AppDbContext>));

                services.Remove(dbContextOptionsDescriptor);

                var dbContextDescriptor = services.Single(
                    d => d.ServiceType ==
                        typeof(AppDbContext));

                services.Remove(dbContextDescriptor);

                services.AddDbContext<AppDbContext>((container, options) =>
                {
                    options.UseInMemoryDatabase("OnlineShop.Api.Test");
                });
            });
        }
    }
}
