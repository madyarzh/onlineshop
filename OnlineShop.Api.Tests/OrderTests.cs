using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using OnlineShop.Api.Responses;
using OnlineShop.Api.Tests.AssertionExceptions;
using OnlineShop.Api.Tests.Extensions;
using OnlineShop.Core.Modules.Buckets.Models;
using OnlineShop.Core.Modules.Items.Models;
using OnlineShop.Core.Modules.Orders.Cqrs;
using OnlineShop.Core.Modules.Orders.Models;
using OnlineShop.Core.Modules.Prices.Models;
using OnlineShop.Core.Shared;
using OnlineShop.Infrastructure.DataAccess;
using System.Text;
using Xunit.Sdk;

namespace OnlineShop.Api.Tests
{
    public class OrderTests : IClassFixture<TestWebApplicationFactory<OnlineShop.Api.Program>>
    {
        private readonly TestWebApplicationFactory<OnlineShop.Api.Program> _factory;

        public OrderTests(TestWebApplicationFactory<Program> factory)
        {
            _factory = factory;

        }

        [Fact]
        public async Task Create_ShouldReturnError_WhenBucketNotFound()
        {
            var httpClient = _factory.CreateClient();

            var item = new Item();
            var bucket = new Bucket()
            {
                BucketItems = new List<BucketItem>
                    {
                        new BucketItem
                        {
                            Item=item,
                            Price=99.9M,
                        },
                    }
            };
            var price = new Price
            {
                Item = item,
                Value = 99.9M
            };
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<AppDbContext>()!;

                context.Items.Add(item);
                context.Buckets.Add(bucket);
                context.Prices.Add(price);
                context.SaveChanges();
            }



            var response = await httpClient.PostAsync("orders/create", new StringContent(JsonConvert.SerializeObject(new CreateOrderRequest
            {
                BucketId = bucket.Id + 1,
                TotalPriceToCheck = 99.9M
            }), Encoding.UTF8, "application/json"));

            Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);
            var responseJson = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseJson);
            var responseError = JsonConvert.DeserializeObject<ErrorResponse>(responseJson);
            Assert.NotNull(responseError);
            Assert.Equal(ErrorCode.entity_not_found.ToString(), responseError.Message);
        }
        [Fact]
        public async Task Create_ShouldReturnOk()
        {
            var httpClient = _factory.CreateClient();

            var item = new Item();
            var bucket = new Bucket()
            {
                BucketItems = new List<BucketItem>
                    {
                        new BucketItem
                        {
                            Item=item,
                            Price=99.9M,
                        },
                    }
            };
            var price = new Price
            {
                Item = item,
                Value = 99.9M
            };
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<AppDbContext>()!;

                context.Items.Add(item);
                context.Buckets.Add(bucket);
                context.Prices.Add(price);
                context.SaveChanges();
            }


            var response = await httpClient.PostAsync("orders/create", new StringContent(JsonConvert.SerializeObject(new CreateOrderRequest
            {
                BucketId = bucket.Id,
                TotalPriceToCheck = 99.9M
            }),Encoding.UTF8,"application/json"));

            var responseText = await response.Content.ReadAsStringAsync();

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new HttpResponseMessageStatusAssertionException(responseText);
            }
            var dataResponse = JsonConvert.DeserializeObject<DataResponse<Order>>(responseText);
            Assert.NotNull(dataResponse);
            Assert.NotNull(dataResponse.Data);
            Assert.NotEqual(0, dataResponse.Data.Id);
        }
        [Fact]
        public async Task Create_ShouldReturnError_WhenTotalPriceIsDifferent()
        {
            var httpClient = _factory.CreateClient();
            var item = new Item();
            var bucket = new Bucket()
            {
                BucketItems = new List<BucketItem>
                    {
                        new BucketItem
                        {
                            Item=item,
                            Price=99.9M,
                        },
                    }
            };
            var price = new Price
            {
                Item = item,
                Value = 99.9M
            };
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<AppDbContext>()!;

                context.Items.Add(item);
                context.Buckets.Add(bucket);
                context.Prices.Add(price);
                context.SaveChanges();
            }

            var response = await httpClient.PostAsync("orders/create", new StringContent(JsonConvert.SerializeObject(new CreateOrderRequest
            {
                BucketId = bucket.Id,
                TotalPriceToCheck = 100M
            }), Encoding.UTF8, "application/json"));


            Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);
            var responseJson = await response.Content.ReadAsStringAsync();
            Assert.NotNull(responseJson);
            var responseError = JsonConvert.DeserializeObject<ErrorResponse>(responseJson);
            Assert.NotNull(responseError);
            Assert.Equal(ErrorCode.price_changed.ToString(), responseError.Message);
        }
    }
}