﻿using Microsoft.EntityFrameworkCore;
using OnlineShop.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShop.Api.Tests.Helpers
{
    public class DbContextHelper
    {
        private readonly DbContext _dbContext;

        public DbContextHelper(DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public int GetExistingId<T>()where T: class, IDbModel
        {
            return _dbContext.Set<T>().First().Id;
        }
        public int GetNotExistingId<T>()where T: class, IDbModel
        {
            return _dbContext.Set<T>().Max(e => e.Id) + 1;
        }
    }
}
